FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04
#8.0-cudnn6-devel-ubuntu16.04
#8.0-devel-ubuntu16.04
LABEL maintainer "Juan Ricardo <juan@santisi.io>"

RUN apt-get update && apt-get install -y --no-install-recommends \
       apt-transport-https \
       apt-utils \
       automake \
       build-essential \
       bzip2 \
       ca-certificates \
       cmake \
       curl \
       gcc \
       g++ \
       git \
       libgeotiff-dev \
       libhdf5-dev \
       libjasper-dev \
       libjpeg-dev \
       libmpich-dev \
       libopenexr-dev \
       libopenimageio-dev \
       libpng12-dev \
       libproj-dev \
       libtbb2 \
       libtbb-dev \
       libtiff5-dev \
       libtool \
       libwebp-dev \
       make \
       mercurial \
       mpich \
       pkg-config \
       software-properties-common \
       subversion \
       vim \
       wget \
       libcurl4-gnutls-dev \
       libpq-dev \
       postgresql-server-dev-all \
       libboost-all-dev \
       libopenmpi-dev \
       libgflags-dev \
       libgoogle-glog-dev \
       zlib1g-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

#
# googletest
#
RUN git clone https://github.com/google/googletest.git && \
      cd  googletest && \
      cmake . && \
      make install && \
      cd .. && \
      rm -r googletest
#
# Eigen
#
RUN hg clone https://bitbucket.org/eigen/eigen && \
        cd eigen && \
        hg update 3.3.4 && \
        mkdir build_dir && \
        cd build_dir && \
        cmake .. && \
        make install && \
        cd ../.. && \
        rm -r eigen

#
# ceres-solver
#
RUN git clone https://github.com/ceres-solver/ceres-solver.git && \
    cd ceres-solver && \
    mkdir ceres-bin && \
    cd ceres-bin && \
    cmake .. \
       -DBUILD_TESTING=OFF \
       -DBUILD_DOCUMENTATION=OFF \
       -DBUILD_EXAMPLES=OFF && \
    make -j7 && \
    make install && \
    cd ../.. && \
    rm -r ceres-solver
#
# OpenCV
#
RUN git clone https://github.com/opencv/opencv.git && \
      cd opencv && \
      git checkout 3.3.0  && \
      mkdir build && \
      cd build && \
      cmake .. && \
      make -j7 && \
      make install && \
      cd  ../..
# Line3D pre-reqs
# googletest
RUN git clone https://github.com/google/googletest.git

RUN wget https://launchpad.net/ubuntu/+archive/primary/+files/libtclap-dev_1.2.1-1_amd64.deb

RUN apt install ./libtclap-dev_1.2.1-1_amd64.deb && \
    rm ./libtclap-dev_1.2.1-1_amd64.deb

RUN git clone https://github.com/Tencent/rapidjson.git && \
    cd rapidjson && \
    mkdir build && \
    cd build && \
    cmake -DGTEST_SOURCE_DIR=/googletest .. && \
    make && \
    make install && \
    cd ../.. && \
    rm -rf rapidjson
#
# Line3Dpp
#
RUN git clone https://github.com/manhofer/Line3Dpp.git && \
    cd Line3Dpp && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make
