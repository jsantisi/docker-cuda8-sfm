# Docker Nvidia #
This container was designed to run Structure-from-Motion (SfM) and Multiple View Stereosis (MVS) software.  The Linux OS - Ubuntu 16.04, we had a hard time getting one of the SfM projects to work with the newer version of Nvidia's CUDA SDK and Driver.

### MVS ###
* Line3Dpp

### Computer Vision ###
* OpenCV

### TODO ###
* Colmap
* Theia-SfM
* OpenMVS (SfM and MVS)

## Installing Nvidia-Docker ##
First, make sure that you have the nvidia-docker installed.  If you have *docker* installed, you will have to uninstall it first.  Follow the instructions on this link.

https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)

## Build the Container ##
Second, build the Docker container:
```
:>docker build -t "tagme" .
```

## Run the Container ##
In order to run the docker container, you need pass the Nvidia runtime.

The *-v* mounts a local directory that contains the images and other meta-data.

The *--runtime=nvidia* tells docker to use the nvidia driver/

![Nvidia Docker ](https://cloud.githubusercontent.com/assets/3028125/12213714/5b208976-b632-11e5-8406-38d379ec46aa.png)
```
docker run -v /home/jsantisi/dev/data:/opt/data -t -i --runtime=nvidia --entrypoint=/bin/bash cuda8-dev
```
## Line3D++ ##
Line3D++ is a line-based Multi-View Stereo (MVS) algorithm written in C++, that can generate 3D line models from oriented image sequences (e.g. Structure-from-Motion results).

Step 1: ``` :> cd Line3D/build ```
Step 2: ``` :>./runLine3Dpp_colmap -i <image_folder> [-m <sfm_result_folder>] ```

Ok - here's where the -v comes in handy.  Replace the <image folder> with /opt/data/[your_images_directory] and -m [your colmap folder with the: cameras.txt, images.txt & points3D.txt]

### View the Results ###
Line3D places the results in the *image directory* under **Line3D++**.

You will need one of the following viewers:
* Cloud Compare - open the *.obj file
* Meshlab - open the *.stl file

The **.txt** file contains the reconstructed 3D lines and their visibility information in the following format (one line in the file per resulting 3D line):

### Additional Nvidia-Docker Commands ###
Run the container and execute the nvidia-smi commnand:
```
sudo docker run --runtime=nvidia <container id> nvidia-smi
```
Run the Standard Nvidia Container:
```
sudo docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi
```
